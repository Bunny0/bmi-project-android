package com.sitthichai.bmiproject

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.sitthichai.bmiproject.databinding.FragmentDescriptionBinding

class DescriptionFragment : Fragment() {

    private var _binding : FragmentDescriptionBinding? = null
    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDescriptionBinding.inflate((inflater, container, false))
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.home?.setOnClickListener {
//            val action = DescriptionFragmentDirections.action_DctDataFragment_to_ListDataFragment
//            view.findNavController().navigate(action)
        }
        binding?.profile?.setOnClickListener {
//            val action = DctDatasFragmentDirections.action_dct_data_to_list_data()
//            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}